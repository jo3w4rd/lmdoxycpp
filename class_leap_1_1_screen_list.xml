<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.8.2">
  <compounddef id="class_leap_1_1_screen_list" kind="class" prot="public">
    <compoundname>Leap::ScreenList</compoundname>
    <basecompoundref prot="public" virt="non-virtual">Leap::Interface</basecompoundref>
      <sectiondef kind="public-type">
      <memberdef kind="typedef" id="class_leap_1_1_screen_list_1aa5375d780cb454e661f94096dcefd431" prot="public" static="no">
        <type>ConstListIterator&lt; ScreenList, Screen &gt;</type>
        <definition>typedef ConstListIterator&lt;ScreenList, Screen&gt; Leap::ScreenList::const_iterator</definition>
        <argsstring></argsstring>
        <name>const_iterator</name>
        <briefdescription>
<para>A C++ iterator type for this ScreenList objects. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3080" bodyfile="/Volumes/LEAP/TestRestructure/source/API/Leap.h" bodystart="3080" bodyend="-1"/>
      </memberdef>
      </sectiondef>
      <sectiondef kind="public-func">
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a416bd76ced1f50c1625b6c780608476b" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>const_iterator</type>
        <definition>const_iterator Leap::ScreenList::begin</definition>
        <argsstring>() const </argsstring>
        <name>begin</name>
        <briefdescription>
<para>The C++ iterator set to the beginning of this ScreenList. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3086"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a9f52fa2e306123f9c8c9efe89a1ac442" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>Screen</type>
        <definition>Screen Leap::ScreenList::closestScreen</definition>
        <argsstring>(const Vector &amp;position) const </argsstring>
        <name>closestScreen</name>
        <param>
          <type>const <ref refid="struct_leap_1_1_vector" kindref="compound">Vector</ref> &amp;</type>
          <declname>position</declname>
        </param>
        <briefdescription>
<para>Gets the Screen closest to the specified position. </para>        </briefdescription>
        <detaileddescription>
<para>The specified position is projected along each screen&apos;s normal vector onto the screen&apos;s plane. The screen whose projected point is closest to the specified position is returned. Call Screen::project(position) on the returned Screen object to find the projected point.</para><para><programlisting><codeline><highlight class="normal"><sp/><sp/><sp/><sp/>Frame<sp/>frame<sp/>=<sp/>leapController.frame();</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp/>(frame.pointables().count()<sp/>&gt;<sp/>0)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>Pointable<sp/>pointable<sp/>=<sp/>frame.pointables()[0];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>ScreenList<sp/>screens<sp/>=<sp/>leapController.locatedScreens();</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>Screen<sp/>screen<sp/>=<sp/>screens.closestScreen(pointable.tipPosition());</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
</programlisting></para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>position</parametername>
</parameternamelist>
<parameterdescription>
<para>The position from which to check for screen projection. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The closest Screen onto which the specified position is projected. </para></simplesect>
<simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3163"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a069798f52ff8f19447e5e8af3edbac61" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>Screen</type>
        <definition>Screen Leap::ScreenList::closestScreenHit</definition>
        <argsstring>(const Pointable &amp;pointable) const </argsstring>
        <name>closestScreenHit</name>
        <param>
          <type>const <ref refid="class_leap_1_1_pointable" kindref="compound">Pointable</ref> &amp;</type>
          <declname>pointable</declname>
        </param>
        <briefdescription>
<para>Gets the closest Screen intercepting a ray projecting from the specified <ref refid="class_leap_1_1_pointable" kindref="compound">Pointable</ref> object. </para>        </briefdescription>
        <detaileddescription>
<para>The projected ray emanates from the <ref refid="class_leap_1_1_pointable" kindref="compound">Pointable</ref> tipPosition along the <ref refid="class_leap_1_1_pointable" kindref="compound">Pointable</ref>&apos;s direction vector. If the projected ray does not intersect any screen surface directly, then the Leap Motion software checks for intersection with the planes extending from the surfaces of the known screens and returns the Screen with the closest intersection.</para><para><programlisting><codeline><highlight class="normal"><sp/><sp/><sp/><sp/>Frame<sp/>frame<sp/>=<sp/>leapController.frame();</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp/>(frame.pointables().count()<sp/>&gt;<sp/>0)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>Pointable<sp/>pointable<sp/>=<sp/>frame.pointables()[0];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>ScreenList<sp/>screens<sp/>=<sp/>leapController.locatedScreens();</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>Screen<sp/>screen<sp/>=<sp/>screens.closestScreenHit(pointable);</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
</programlisting></para><para>If no intersections are found (i.e. the ray is directed parallel to or away from all known screens), then an invalid Screen object is returned.</para><para><emphasis>Note:</emphasis> Be sure to test whether the Screen object returned by this method is valid. Attempting to use an invalid Screen object will lead to incorrect results.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>pointable</parametername>
</parameternamelist>
<parameterdescription>
<para>The <ref refid="class_leap_1_1_pointable" kindref="compound">Pointable</ref> object to check for screen intersection. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The closest Screen toward which the specified <ref refid="class_leap_1_1_pointable" kindref="compound">Pointable</ref> object is pointing, or, if the pointable is not pointing in the direction of any known screen, an invalid Screen object. </para></simplesect>
<simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3119"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1ab6a0b5863493e8cd6b6e66e1c799486c" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>Screen</type>
        <definition>Screen Leap::ScreenList::closestScreenHit</definition>
        <argsstring>(const Vector &amp;position, const Vector &amp;direction) const </argsstring>
        <name>closestScreenHit</name>
        <param>
          <type>const <ref refid="struct_leap_1_1_vector" kindref="compound">Vector</ref> &amp;</type>
          <declname>position</declname>
        </param>
        <param>
          <type>const <ref refid="struct_leap_1_1_vector" kindref="compound">Vector</ref> &amp;</type>
          <declname>direction</declname>
        </param>
        <briefdescription>
<para>Gets the closest Screen intercepting a ray projecting from the specified position in the specified direction. </para>        </briefdescription>
        <detaileddescription>
<para>The projected ray emanates from the position along the direction vector. If the projected ray does not intersect any screen surface directly, then the Leap Motion software checks for intersection with the planes extending from the surfaces of the known screens and returns the Screen with the closest intersection.</para><para><programlisting><codeline><highlight class="normal"><sp/><sp/><sp/><sp/>Frame<sp/>frame<sp/>=<sp/>leapController.frame();</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp/>(frame.pointables().count()<sp/>&gt;<sp/>0)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>Pointable<sp/>pointable<sp/>=<sp/>frame.pointables()[0];</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>ScreenList<sp/>screens<sp/>=<sp/>leapController.locatedScreens();</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/><sp/><sp/>Screen<sp/>screen<sp/>=<sp/>screens.closestScreen(pointable.tipPosition());</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
</programlisting></para><para>If no intersections are found (i.e. the ray is directed parallel to or away from all known screens), then an invalid Screen object is returned.</para><para><emphasis>Note:</emphasis> Be sure to test whether the Screen object returned by this method is valid. Attempting to use an invalid Screen object will lead to incorrect results.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>position</parametername>
</parameternamelist>
<parameterdescription>
<para>The position from which to check for screen intersection. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>direction</parametername>
</parameternamelist>
<parameterdescription>
<para>The direction in which to check for screen intersection. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The closest Screen toward which the specified ray is pointing, or, if the ray is not pointing in the direction of any known screen, an invalid Screen object. </para></simplesect>
<simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3147"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a5a62705f1be43cd6069f083dd4e53f38" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>int</type>
        <definition>int Leap::ScreenList::count</definition>
        <argsstring>() const </argsstring>
        <name>count</name>
        <briefdescription>
<para>Returns the number of screens in this list. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>The number of screens in this list. </para></simplesect>
<simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3059"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a23a5564b75264594487828904c3dbe23" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>const_iterator</type>
        <definition>const_iterator Leap::ScreenList::end</definition>
        <argsstring>() const </argsstring>
        <name>end</name>
        <briefdescription>
<para>The C++ iterator set to the end of this ScreenList. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3092"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a7ba8102369fbca39482db7fdeca55db7" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>bool</type>
        <definition>bool Leap::ScreenList::isEmpty</definition>
        <argsstring>() const </argsstring>
        <name>isEmpty</name>
        <briefdescription>
<para>Reports whether the list is empty. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>True, if the list has no members. </para></simplesect>
<simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3066"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a8e1f70dbf9a2e636840337ae6ff3dee1" prot="public" static="no" const="yes" explicit="no" inline="no" virt="non-virtual">
        <type>Screen</type>
        <definition>Screen Leap::ScreenList::operator[]</definition>
        <argsstring>(int index) const </argsstring>
        <name>operator[]</name>
        <param>
          <type>int</type>
          <declname>index</declname>
        </param>
        <briefdescription>
<para>Access a list member by its position in the list. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>index</parametername>
</parameternamelist>
<parameterdescription>
<para>The zero-based list position index. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The Screen object at the specified index. </para></simplesect>
<simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3074"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1a7ce73ebf3b8f5d85f456417a3fd5227c" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type></type>
        <definition>Leap::ScreenList::ScreenList</definition>
        <argsstring>(const ListBaseImplementation&lt; Screen &gt; &amp;)</argsstring>
        <name>ScreenList</name>
        <param>
          <type>const ListBaseImplementation&lt; Screen &gt; &amp;</type>
        </param>
        <briefdescription>
        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3046"/>
      </memberdef>
      <memberdef kind="function" id="class_leap_1_1_screen_list_1af233b44c1a71602bc38c8af27089b71d" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type></type>
        <definition>Leap::ScreenList::ScreenList</definition>
        <argsstring>()</argsstring>
        <name>ScreenList</name>
        <briefdescription>
<para>Constructs an empty list of screens. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="since"><para>1.0 </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3052"/>
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <location file="/Volumes/LEAP/TestRestructure/source/API/Leap.h" line="3043" bodyfile="/Volumes/LEAP/TestRestructure/source/API/Leap.h" bodystart="3043" bodyend="3164"/>
    <listofallmembers>
      <member refid="class_leap_1_1_screen_list_1a416bd76ced1f50c1625b6c780608476b" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>begin</name></member>
      <member refid="class_leap_1_1_screen_list_1a9f52fa2e306123f9c8c9efe89a1ac442" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>closestScreen</name></member>
      <member refid="class_leap_1_1_screen_list_1a069798f52ff8f19447e5e8af3edbac61" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>closestScreenHit</name></member>
      <member refid="class_leap_1_1_screen_list_1ab6a0b5863493e8cd6b6e66e1c799486c" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>closestScreenHit</name></member>
      <member refid="class_leap_1_1_screen_list_1aa5375d780cb454e661f94096dcefd431" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>const_iterator</name></member>
      <member refid="class_leap_1_1_screen_list_1a5a62705f1be43cd6069f083dd4e53f38" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>count</name></member>
      <member refid="class_leap_1_1_screen_list_1a23a5564b75264594487828904c3dbe23" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>end</name></member>
      <member refid="class_leap_1_1_interface_1af5e59f158a1738b37b64d5161fad550a" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>get</name></member>
      <member refid="class_leap_1_1_interface_1afc3b49f03cf244fe5fde7d88ba9fc76e" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>Interface</name></member>
      <member refid="class_leap_1_1_interface_1a4a12eb3b57d16cc468c6ded94091f486" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>Interface</name></member>
      <member refid="class_leap_1_1_interface_1a6a1e8b70d3990465b4d81f0912f7ef48" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>Interface</name></member>
      <member refid="class_leap_1_1_interface_1a48b7660b77d962b30fa397919901967b" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>Interface</name></member>
      <member refid="class_leap_1_1_screen_list_1a7ba8102369fbca39482db7fdeca55db7" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>isEmpty</name></member>
      <member refid="class_leap_1_1_interface_1aebf4e096301998b829239acd895b387c" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>m_object</name></member>
      <member refid="class_leap_1_1_interface_1a568404674abb31dd7419d064f229b631" prot="protected" virt="non-virtual"><scope>Leap::ScreenList</scope><name>operator=</name></member>
      <member refid="class_leap_1_1_screen_list_1a8e1f70dbf9a2e636840337ae6ff3dee1" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>operator[]</name></member>
      <member refid="class_leap_1_1_screen_list_1a7ce73ebf3b8f5d85f456417a3fd5227c" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>ScreenList</name></member>
      <member refid="class_leap_1_1_screen_list_1af233b44c1a71602bc38c8af27089b71d" prot="public" virt="non-virtual"><scope>Leap::ScreenList</scope><name>ScreenList</name></member>
      <member refid="class_leap_1_1_interface_1a12d2f007f6ac6ea5ffde26a9e768223e" prot="protected" virt="virtual"><scope>Leap::ScreenList</scope><name>~Interface</name></member>
    </listofallmembers>
  </compounddef>
</doxygen>
